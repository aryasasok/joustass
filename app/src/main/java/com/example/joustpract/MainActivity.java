package com.example.joustpract;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;


public class MainActivity extends AppCompatActivity {

    GameEngine Joust;
    int screenwidth;
    int screenheight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get size of the screen
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenwidth = size.x;
                screenheight= size.y/4;

                Joust = new GameEngine(this, screenwidth,screenheight);
                setContentView(Joust);

        // Initialize the GameEngine object
        // Pass it the screen size (height & width)
        Joust = new GameEngine(this, size.x, size.y);

        // Make GameEngine the view of the Activity
        setContentView(Joust);
    }

    // Android Lifecycle function
    @Override
    protected void onResume() {
        super.onResume();
        Joust.startGame();
    }

    // Stop the thread in snakeEngine
    @Override
    protected void onPause() {
        super.onPause();
        Joust.pauseGame();
    }
    public  static int getHeight(AppCompatActivity context){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }
    public static int getWidth(AppCompatActivity context){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }
}



